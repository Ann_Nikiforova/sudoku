#include <dopfunctions.h>
#include <workprognosis.h>


// перенести значения из массива Клеток в массив цифр
void transferInArray(Cell masCells[9][9], int masDigits[9][9])
{
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            masDigits[x][y]=masCells[x][y].value;
}



// вычислить следующую строку
int rowCalculate(int i, int j)
{
    if (i%2==0 && j==8)
        i++;
    else if (i%2!=0 && j==0)
        i++;

    return i;
}

// вычислить следующий столбец
int columnCalculate(int i, int j)
{
    if (i%2==0)
    {
        j++;
        if (j==9)
            j=8;
    }
    else if (i%2!=0)
    {
        j--;
        if (j==-1)
            j=0;
    }

    return j;
}

// Проверка на ошибки
bool isErrors(Cell field[9][9], int val, int i, int j)
{
    for (int k=0; k<9; k++)
    {
        if (field[i][k].value==val)
            return true;
        if (field[k][j].value==val)
            return true;
    }
    // исключаем цифры из общего блока
    bool temp=false;
    if (i <=2)
    {
        if (j <= 2) // первый блок
            temp=findErrorInBlock(0, 3, 0, 3, val, field);
        else if (j >=3 && j <= 5) // второй блок
            temp=findErrorInBlock(0, 3, 3, 6, val, field);
        else if (j >=6 && j <= 8) // третий блок
            temp=findErrorInBlock(0, 3, 6, 9, val, field);
    }
    else if (i <= 5 && i >= 3)
    {
        if (j <= 2) // четвертый блок
            temp=findErrorInBlock(3, 6, 0, 3, val, field);
        else if (j >=3 && j <= 5) // пятый блок
            temp=findErrorInBlock(3, 6, 3, 6, val, field);
        else if (j >=6 && j <= 8) // шестой блок
            temp=findErrorInBlock(3, 6, 6, 9, val, field);
    }
    else if (i <= 8 && i >= 6)
    {
        if (j <= 2) // седьдой блок
            temp=findErrorInBlock(6, 9, 0, 3, val, field);
        else if (j >=3 && j <= 5) // восьмой блок
            temp=findErrorInBlock(6, 9, 3, 6, val, field);
        else if (j >=6 && j <= 8) // девятый блок
            temp=findErrorInBlock(6, 9, 6, 9, val, field);
    }
    if (temp==true)
        return true;
    return false;
}

bool findErrorInBlock(int x1, int x2, int y1, int y2, int val, Cell field[9][9])
{
    int proverka=0;
    for(int x=x1; x<x2; x++)
        for (int y=y1; y<y2; y++)
            if (field[x][y].value==val)
                proverka++;
    if (proverka > 1)
        return true;
    return false;
}

// Вычислить координаты предыдущей клетки
// строка
int rowPrevious(int i, int j)
{
    if (i%2==0 && j==0)
        i--;
    else if (i%2!=0 && j==8)
        i--;

    return i;
}

// Вычислить координаты предыдущей клетки
// столбец
int columnPrevious(int i, int j)
{
    if (i%2==0 && j==0)
        j=0;
    else if (i%2==0)
        j--;
    else if (i%2!=0 && j==8)
        j=8;
    else if (i%2!=0)
        j++;

    return j;
}
