#ifndef TEST_ENUMERATIONVALUESINPROGNOSIS_H
#define TEST_ENUMERATIONVALUESINPROGNOSIS_H
#pragma once

#include <QtTest/QTest>
#include <QtGui>
#include <cell.h>
#include "mainwindow.h"
#include <dopfunctions.h>
#include <workprognosis.h>

class test_enumerationValuesInPrognosis : public QObject
{
    Q_OBJECT
public:
    test_enumerationValuesInPrognosis();
    ~test_enumerationValuesInPrognosis();

    // идеальная головоломка
    int idealMas[9][9]=
    {
        {8,6,3, 4,9,5, 1,2,7},
        {1,2,5, 3,6,7, 9,8,4},
        {4,9,7, 2,8,1, 5,3,6},
        {7,3,6, 5,4,9, 8,1,2},
        {9,1,8, 6,7,2, 4,5,3},
        {2,5,4, 8,1,3, 7,6,9},
        {3,4,1, 9,2,8, 6,7,5},
        {5,8,9, 7,3,6, 2,4,1},
        {6,7,2, 1,5,4, 3,9,8}
    };
private slots:
    // Вариант 1
    void var_1_easy();
    // Вариант 2
    void var_2_medium();
    // Вариант 3
    void var_3_hard();
};

#endif // TEST_ENUMERATIONVALUESINPROGNOSIS_H
