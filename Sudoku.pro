#-------------------------------------------------
#
# Project created by QtCreator 2015-07-04T00:09:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Sudoku
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cell.cpp \
    test_openeddigits.cpp \
    test_hiddendigits.cpp \
    dopfunctions.cpp \
    test_enumerationvaluesinprognosis.cpp \
    workprognosis.cpp

HEADERS  += mainwindow.h \
    cell.h \
    test_openeddigits.h \
    test_hiddendigits.h \
    dopfunctions.h \
    test_enumerationvaluesinprognosis.h \
    workprognosis.h

FORMS    += mainwindow.ui
QT += testlib
