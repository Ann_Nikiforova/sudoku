#include "test_hiddendigits.h"

test_hiddenDigits::test_hiddenDigits()
{

}

test_hiddenDigits::~test_hiddenDigits()
{

}


// клетка в верхнем левом углу блока
void test_hiddenDigits::cellIsInUpperLeftCornerBlock()
{
    // исходная головоломка
    int startMas[9][9]=
    {
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,7,0, 0,0,0},
        {0,0,0, 0,0,0, 7,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,7,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,7, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0}
    };

    // идеальный результат
    int resultMas[9][9];
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            resultMas[x][y]=startMas[x][y];
    resultMas[0][0]=7;

    // заполняем массив клеток
    Cell startMasCells[9][9];
    int indexCell=1;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            startMasCells[x][y].value=startMas[x][y];
            startMasCells[x][y].index=indexCell;
            indexCell++;
        }
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.hiddenDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

// клетка в верхнем правом углу блока
void test_hiddenDigits::cellIsInUpperRightCornerBlock()
{
    // исходная головоломка
    int startMas[9][9]=
    {
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {3,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,3,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,3, 0,0,0},
        {0,0,0, 0,0,0, 0,3,0}
    };

    // идеальный результат
    int resultMas[9][9];
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            resultMas[x][y]=startMas[x][y];
    resultMas[6][2]=3;

    // заполняем массив клеток
    Cell startMasCells[9][9];
    int indexCell=1;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            startMasCells[x][y].value=startMas[x][y];
            startMasCells[x][y].index=indexCell;
            indexCell++;
        }
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.hiddenDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

// клетка в нижнем левом углу блока
void test_hiddenDigits::cellIsInLowerLeftCornerBlock()
{
    // исходная головоломка
    int startMas[9][9]=
    {
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,2, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {2,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,2,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,2,0, 0,0,0}
    };

    // идеальный результат
    int resultMas[9][9];
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            resultMas[x][y]=startMas[x][y];
    resultMas[5][3]=2;

    // заполняем массив клеток
    Cell startMasCells[9][9];
    int indexCell=1;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            startMasCells[x][y].value=startMas[x][y];
            startMasCells[x][y].index=indexCell;
            indexCell++;
        }
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.hiddenDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

// клетка в нижнем правом углу блока
void test_hiddenDigits::cellIsInLowerRightCornerBlock()
{
    // исходная головоломка
    int startMas[9][9]=
    {
        {0,0,0, 1,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,1,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,1,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,1,0},
        {0,0,0, 0,0,0, 0,0,0}
    };

    // идеальный результат
    int resultMas[9][9];
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            resultMas[x][y]=startMas[x][y];
    resultMas[8][5]=1;

    // заполняем массив клеток
    Cell startMasCells[9][9];
    int indexCell=1;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            startMasCells[x][y].value=startMas[x][y];
            startMasCells[x][y].index=indexCell;
            indexCell++;
        }
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.hiddenDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

// клетка с левой стороны блока
void test_hiddenDigits::cellLocatedLeftSideBlock()
{
    // исходная головоломка
    int startMas[9][9]=
    {
        {0,0,0, 0,0,0, 0,0,0},
        {0,4,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 4,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,4,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,4, 0,0,0, 0,0,0}
    };

    // идеальный результат
    int resultMas[9][9];
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            resultMas[x][y]=startMas[x][y];
    resultMas[4][0]=4;

    // заполняем массив клеток
    Cell startMasCells[9][9];
    int indexCell=1;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            startMasCells[x][y].value=startMas[x][y];
            startMasCells[x][y].index=indexCell;
            indexCell++;
        }
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.hiddenDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

// клетка с правой стороны блока
void test_hiddenDigits::cellLocatedRightSideBlock()
{
    // исходная головоломка
    int startMas[9][9]=
    {
        {0,0,0, 0,6,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,6,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 6,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,6,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0}
    };

    // идеальный результат
    int resultMas[9][9];
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            resultMas[x][y]=startMas[x][y];
    resultMas[1][8]=6;

    // заполняем массив клеток
    Cell startMasCells[9][9];
    int indexCell=1;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            startMasCells[x][y].value=startMas[x][y];
            startMasCells[x][y].index=indexCell;
            indexCell++;
        }
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.hiddenDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

// клетка с верхней стороны блока
void test_hiddenDigits::cellIsTopSideBlock()
{
    // исходная головоломка
    int startMas[9][9]=
    {
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 8,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,8, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {8,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 8,0,0}
    };

    // идеальный результат
    int resultMas[9][9];
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            resultMas[x][y]=startMas[x][y];
    resultMas[6][4]=8;

    // заполняем массив клеток
    Cell startMasCells[9][9];
    int indexCell=1;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            startMasCells[x][y].value=startMas[x][y];
            startMasCells[x][y].index=indexCell;
            indexCell++;
        }
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.hiddenDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

// клетка с нижней стороны блока
void test_hiddenDigits::cellIsBottomSideBlock()
{
    // исходная головоломка
    int startMas[9][9]=
    {
        {0,0,0, 0,0,0, 9,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,9},
        {9,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,9,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0}
    };

    // идеальный результат
    int resultMas[9][9];
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            resultMas[x][y]=startMas[x][y];
    resultMas[8][7]=9;

    // заполняем массив клеток
    Cell startMasCells[9][9];
    int indexCell=1;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            startMasCells[x][y].value=startMas[x][y];
            startMasCells[x][y].index=indexCell;
            indexCell++;
        }
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.hiddenDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

// клетка в центре блока
void test_hiddenDigits::cellIsCenterBlock()
{
    // исходная головоломка
    int startMas[9][9]=
    {
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,5, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {5,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,5,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 5,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0}
    };

    // идеальный результат
    int resultMas[9][9];
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            resultMas[x][y]=startMas[x][y];
    resultMas[4][4]=5;

    // заполняем массив клеток
    Cell startMasCells[9][9];
    int indexCell=1;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            startMasCells[x][y].value=startMas[x][y];
            startMasCells[x][y].index=indexCell;
            indexCell++;
        }
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.hiddenDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

// несколько клеток
void test_hiddenDigits::fewCells()
{
    // исходная головоломка
    int startMas[9][9]=
    {
        {0,0,0, 0,1,0, 0,0,0},
        {2,0,0, 0,0,0, 0,0,0},
        {0,0,0, 3,0,2, 0,1,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,1, 0,0,0, 0,0,0},
        {0,0,0, 0,0,3, 2,0,0},
        {0,0,0, 0,0,0, 0,3,0},
        {1,0,0, 0,0,0, 0,0,0},
        {0,3,0, 0,0,0, 0,0,2}
    };

    // идеальный результат
    int resultMas[9][9];
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            resultMas[x][y]=startMas[x][y];
    resultMas[1][1]=1;
    resultMas[7][4]=3;
    resultMas[0][7]=2;

    // заполняем массив клеток
    Cell startMasCells[9][9];
    int indexCell=1;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            startMasCells[x][y].value=startMas[x][y];
            startMasCells[x][y].index=indexCell;
            indexCell++;
        }
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.hiddenDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

