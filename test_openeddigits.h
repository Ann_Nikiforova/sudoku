#ifndef TEST_OPENEDDIGITS_H
#define TEST_OPENEDDIGITS_H
#pragma once

#include <QtTest/QTest>
#include <QtGui>
#include <cell.h>
#include "mainwindow.h"
#include <dopfunctions.h>
#include <workprognosis.h>

class test_openedDigits : public QObject
{
    Q_OBJECT
public:
    test_openedDigits();
    ~test_openedDigits();

    // идеальная головоломка
    int resultMas[9][9]=
    {
        {8,6,3, 4,9,5, 1,2,7},
        {1,2,5, 3,6,7, 9,8,4},
        {4,9,7, 2,8,1, 5,3,6},
        {7,3,6, 5,4,9, 8,1,2},
        {9,1,8, 6,7,2, 4,5,3},
        {2,5,4, 8,1,3, 7,6,9},
        {3,4,1, 9,2,8, 6,7,5},
        {5,8,9, 7,3,6, 2,4,1},
        {6,7,2, 1,5,4, 3,9,8}
    };

private slots:
    // Нет пустых клеток с единственной цифрой в прогнозе
    void noEmptyCellsWithSingleDigitInPrognosis();
    // Только одна клетка с единственной цифрой в прогнозе
    void onlyOneCell();
    // Клетка находится на правой границе поля
    void cellLocatedRightEdgeField();
    // Клетка находится на нижней границе поля
    void cellLocatedLowerBoundaryEdgeField();
    // Клетка находится в правом нижнем углу поля
    void cellIsInBottomRightCornerField();
    // Несколько клеток с единственной цифрой в прогнозе (вариант 1)
    void fewCells_1();
    // Несколько клеток с единственной цифрой в прогнозе (вариант 2)
    void fewCells_2();
};

#endif // TEST_OPENEDDIGITS_H
