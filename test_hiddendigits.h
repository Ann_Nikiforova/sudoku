#ifndef TEST_HIDDENDIGITS_H
#define TEST_HIDDENDIGITS_H
#pragma once

#include <QtTest/QTest>
#include <QtGui>
#include <dopfunctions.h>
#include <workprognosis.h>
#include "mainwindow.h"
#include <cell.h>
#include <QDebug>


class test_hiddenDigits : public QObject
{
    Q_OBJECT
public:
    test_hiddenDigits();
    ~test_hiddenDigits();

private slots:
    // клетка в верхнем левом углу блока
    void cellIsInUpperLeftCornerBlock();
    // клетка в верхнем правом углу блока
    void cellIsInUpperRightCornerBlock();
    // клетка в нижнем левом углу блока
    void cellIsInLowerLeftCornerBlock();
    // клетка в нижнем правом углу блока
    void cellIsInLowerRightCornerBlock();
    // клетка с левой стороны блока
    void cellLocatedLeftSideBlock();
    // клетка с правой стороны блока
    void cellLocatedRightSideBlock();
    // клетка с верхней стороны блока
    void cellIsTopSideBlock();
    // клетка с нижней стороны блока
    void cellIsBottomSideBlock();
    // клетка в центре блока
    void cellIsCenterBlock();
    // несколько клеток
    void fewCells();
};

#endif // TEST_HIDDENDIGITS_H
