#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->solveBtn, SIGNAL(clicked()), this, SLOT(SolvingSudoku()));
    connect(ui->clearBtn, SIGNAL(clicked()), this, SLOT(ClearTheValues()));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::SolvingSudoku()
{
    loadDataCells(field); // загрузка данных
    if (isEmptyField(field)==false) // проверка на заполнение поля
    {
        if (findErrors(field)==false && isOneDigitInCell(field)==true)
        {
        while (isChangedTable)
        {
            while (isChangedTable)
                openedDigits(field); // метод открытых цифр
            hiddenDigits(field); // метод скрытых цифр
        }
        enumerationValuesInPrognosis(field, 0, 0); // перебор
        outputResult(field); // вывод результата
        }
    }
}

void MainWindow::ClearTheValues()
{
    for(int i=0; i<9; i++)
        for(int j=0; j<9; j++)
            ui->poleTable->item(i,j)->setText("");
}

void MainWindow::loadDataCells(Cell field[9][9])
{
    int indexCell=1;
    for(int i=0; i<9; i++)
    {
        for(int j=0; j<9; j++)
        {
            ui->poleTable->setCurrentCell(i,j); // переход на нужную клетку в таблице
            field[i][j].index=indexCell; // записываем индекс
            indexCell++;
            field[i][j].value=ui->poleTable->currentItem()->text().toInt(); // записываем значение поля
        }
    }
    // обновляем прогноз
    fillPrognosis(field);
}

void MainWindow::openedDigits(Cell field[9][9])
{
    isChangedTable=false;
    for(int i=0; i<9; i++)
    {
        for(int j=0; j<9; j++)
        {
            if (field[i][j].value==0) // если клетка пустая
            {
                if (field[i][j].prognosis.count()==1) // если в прогнозе одно значение
                {
                    field[i][j].value=field[i][j].prognosis[0];
                    isChangedTable=true;
                }
                // обновляем прогноз
                fillPrognosis(field);
            }
        }
    }
}

void MainWindow::hiddenDigits(Cell field[9][9])
{
    isChangedTable=false;
    int prognVal=0;
    for (int i=0; i<9; i++)
    {
        for (int j=0; j<9; j++)
        {
            if (field[i][j].value==0) // если пустая клетка
            {
                int index=field[i][j].index;
                // если клетка находится в левом верхнем углу блока
                if (index==1 || index==4 || index==7 || index==28 || index==31 || index==34 || index==55 || index==58 || index==61)
                    prognVal=findFourCoincidences(field, i, j, i+1, i+2, j+1, j+2);
                // если клетка находится в вехнем правом углу блока
                else if (index==3 || index==6 || index==9 || index==30 || index==33 || index==36 || index==57 || index==60 || index==63)
                    prognVal=findFourCoincidences(field, i, j, i+1, i+2, j-2, j-1);
                // если клетка находится в нижнем левом углу блока
                else if (index==19 || index==22 || index==25 || index==46 || index==49 || index==52 || index==73 || index==76 || index==79)
                    prognVal=findFourCoincidences(field, i, j, i-2, i-1, j+1, j+2);
                //  если клетка находится в нижнем правом углу блока
                else if (index==21 || index==24 || index==27 || index==48 || index==51 || index==54 || index==75 || index==78 || index==81)
                    prognVal=findFourCoincidences(field, i, j, i-2, i-1, j-2, j-1);
                // если клетка находится у левой границы блока
                else if (index==10 || index==13 || index==16 || index==37 || index==40 || index==43 || index==64 || index==67 || index==70)
                    prognVal=findFourCoincidences(field, i, j, i-1, i+1, j+1, j+2);
                // если клетка находится у правой границы блока
                else if (index==12 || index==15 || index==18 || index==39 || index==42 || index==45 || index==66 || index==69 || index==72)
                    prognVal=findFourCoincidences(field, i, j, i-1, i+1, j-2, j-1);
                // если клетка находится у вехней границы блока
                else if (index==2 || index==5 || index==8 || index==29 || index==32 || index==35 || index==56 || index==59 || index==62)
                    prognVal=findFourCoincidences(field, i, j, i+1, i+2, j-1, j+1);
                // если клетка находится у нижней границы блока
                else if (index==20 || index==23 || index==26 || index==47 || index==50 || index==53 || index==74 || index==77 || index==80)
                    prognVal=findFourCoincidences(field, i, j, i-2, i-1, j-1, j+1);
                // если клетка находится в центре блока
                else if (index==11 || index==14 || index==17 || index==38 || index==41 || index==44 || index==65 || index==68 || index==71)
                    prognVal=findFourCoincidences(field, i, j, i-1, i+1, j-1, j+1);
                // если сработал метод Скрытых цифр
                if (prognVal!=0)
                {
                    isChangedTable=true;
                    // заполняем значение клетки
                    field[i][j].value=prognVal;
                    // обновляем прогноз
                    fillPrognosis(field);
                }
            }
        }
    }
}

void MainWindow::enumerationValuesInPrognosis(Cell field[9][9], int i, int j)
{
        if (field[i][j].value==0)
        {
            for (int k=0; i!=9 && k < field[i][j].prognosis.count(); k++)
            {
                // если значение из прогноза подходит
                if (!(isErrors(field, field[i][j].prognosis[k], i, j)))
                {
                    field[i][j].value=field[i][j].prognosis[k];
                    // находим координаты следующей клетки
                    int x=i, y=j;
                    i=rowCalculate(x, y);
                    j=columnCalculate(x, y);
                    // обновляем список кандидатов в клетках
                    fillPrognosis(field);
                    // рекурсивный вызов
                    if (i!=9)
                        enumerationValuesInPrognosis(field, i, j);
                }
            }
            if (i!=9 && field[i][j].prognosis.count()==0)
            {
                int proverka=0;
                while (proverka==0 && i!=-1)
                {
                // обнуляем значение в клетке
                    field[i][j].value=0;
                // переходим на предыдущую клетку
                    int x=i, y=j;
                    i=rowPrevious(x, y);
                    j=columnPrevious(x, y);
                // запоминаем значение
                    int valCell=0, valProgn=0;
                    valCell=field[i][j].value;
                // ищем это значение в прогнозе
                    for (int k=0; k < field[i][j].prognosis.count(); k++)
                        if (field[i][j].prognosis[k]==valCell)
                            valProgn=k;
                    for (valProgn; valProgn < field[i][j].prognosis.count(); valProgn++)
                    {   // если можно поставить следующее значение
                        if (!(isErrors(field, field[i][j].prognosis[valProgn], i, j)))
                        {
                            // записываем значение в клетку
                            field[i][j].value=field[i][j].prognosis[valProgn];
                            // обновляем список кандидатов в клетках
                            fillPrognosis(field);
                            // рекурсивный вызов
                            enumerationValuesInPrognosis(field, x, y);
                            proverka=1;
                        }
                    }
                }
            }
        }
        else
        {
            // находим координаты следующей клетки
            int x=i, y=j;
            i=rowCalculate(x, y);
            j=columnCalculate(x, y);
            enumerationValuesInPrognosis(field, i, j);
        }
}

void MainWindow::outputResult(Cell field[9][9])
{
    for (int i=0; i<9; i++)
        for (int j=0; j<9; j++)
            ui->poleTable->item(i,j)->setText(QString::number(field[i][j].value));
}

bool MainWindow::findErrors(Cell field[9][9])
{
    for (int i=0; i<9; i++)
        for (int j=0; j<9; j++)
        {
            if (field[i][j].value!=0)
            {
                for (int k=0; k<9; k++)
                {
                    if (field[i][k].value==field[i][j].value && k!=j)
                    {
                        QMessageBox::warning(this,"Ошибка!", "Поле заполнено не по правилам");
                        return true;
                    }
                    if (field[k][j].value==field[i][j].value && k!=i)
                    {
                        QMessageBox::warning(this,"Ошибка!", "Поле заполнено не по правилам");
                        return true;
                    }
                }
                bool ok=false;
                // исключаем цифры из общего блока
                if (i <=2)
                {
                    if (j <= 2) // первый блок
                        ok=isErrorInBlock(0, 3, 0, 3, field[i][j].value, field);
                    else if (j >=3 && j <= 5) // второй блок
                        ok=isErrorInBlock(0, 3, 3, 6, field[i][j].value, field);
                    else if (j >=6 && j <= 8) // третий блок
                        ok=isErrorInBlock(0, 3, 6, 9, field[i][j].value, field);
                }
                else if (i <= 5 && i >= 3)
                {
                    if (j <= 2) // четвертый блок
                        ok=isErrorInBlock(3, 6, 0, 3, field[i][j].value, field);
                    else if (j >=3 && j <= 5) // пятый блок
                        ok=isErrorInBlock(3, 6, 3, 6, field[i][j].value, field);
                    else if (j >=6 && j <= 8) // шестой блок
                        ok=isErrorInBlock(3, 6, 6, 9, field[i][j].value, field);
                }
                else if (i <= 8 && i >= 6)
                {
                    if (j <= 2) // седьдой блок
                        ok=findErrorInBlock(6, 9, 0, 3, field[i][j].value, field);
                    else if (j >=3 && j <= 5) // восьмой блок
                        ok=findErrorInBlock(6, 9, 3, 6, field[i][j].value, field);
                    else if (j >=6 && j <= 8) // девятый блок
                        ok=findErrorInBlock(6, 9, 6, 9, field[i][j].value, field);
                }
                if (ok==true)
                    return true;
            }
        }
    return false;
}

bool MainWindow::isEmptyField(Cell field[9][9])
{
    for(int i=0; i<9; i++)
        for (int j=0; j<9; j++)
            if (field[i][j].value!=0)
            {
                return false;
            }
    QMessageBox::warning(this,"Ошибка! Поле пустое", "Введите начальные значения");
    return true;
}

bool MainWindow::isSolvedSudoku(Cell field[9][9])
{
    for(int i=0; i<9; i++)
        for (int j=0; j<9; j++)
            if (field[i][j].value==0)
            {
                return false;
            }
    return true;
}

bool MainWindow::isErrorInBlock(int x1, int x2, int y1, int y2, int val, Cell field[9][9])
{
    int proverka=0;
    for(int x=x1; x<x2; x++)
        for (int y=y1; y<y2; y++)
            if (field[x][y].value==val)
                proverka++;

    if (proverka > 1)
    {
        QMessageBox::warning(this,"Ошибка!", "Поле заполнено не по правилам");
        return true;
    }
    return false;
}

bool MainWindow::isOneDigitInCell(Cell field[9][9])
{
    for(int i=0; i<9; i++)
        for (int j=0; j<9; j++)
            if (field[i][j].value < 0 || field[i][j].value > 9)
            {
                QMessageBox::warning(this,"Ошибка!", "В клетку можно вводить только однозначное число");
                return false;
            }
    return true;
}

