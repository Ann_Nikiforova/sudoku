#include <workprognosis.h>

// заполнить прогноз
void fillPrognosis(Cell field[9][9])
{
    for(int i=0; i<9; i++)
    {
        for(int j=0; j<9; j++)
        {
            int masProgn[9]={1,2,3,4,5,6,7,8,9};
            // заполняем прогноз
            if (field[i][j].value==0) // если пустая клетка
            {
                for(int k=0; k<9; k++)
                {
                    for(int h=0; h<9; h++)
                    {
                        if (masProgn[h]==field[i][k].value && masProgn[h]!=0) // исключаем цифры из общей строки
                            masProgn[h]=0;
                        if (masProgn[h]==field[k][j].value && masProgn[h]!=0) // исключаем цифры из общего столбца
                            masProgn[h]=0;
                    }
                }
                // исключаем цифры из общего блока
                if (i <=2)
                {
                    if (j <= 2) // первый блок
                        block(0, 3, 0, 3, masProgn, field);
                    else if (j >=3 && j <= 5) // второй блок
                        block(0, 3, 3, 6, masProgn, field);
                    else if (j >=6 && j <= 8) // третий блок
                        block(0, 3, 6, 9, masProgn, field);
                }
                else if (i <= 5 && i >= 3)
                {
                    if (j <= 2) // четвертый блок
                        block(3, 6, 0, 3, masProgn, field);
                    else if (j >=3 && j <= 5) // пятый блок
                        block(3, 6, 3, 6, masProgn, field);
                    else if (j >=6 && j <= 8) // шестой блок
                        block(3, 6, 6, 9, masProgn, field);
                }
                else if (i <= 8 && i >= 6)
                {
                    if (j <= 2) // седьдой блок
                        block(6, 9, 0, 3, masProgn, field);
                    else if (j >=3 && j <= 5) // восьмой блок
                        block(6, 9, 3, 6, masProgn, field);
                    else if (j >=6 && j <= 8) // девятый блок
                        block(6, 9, 6, 9, masProgn, field);
                }
                // записываем оставшиеся цифры в прогноз
                field[i][j].prognosis.clear(); // очищаем старый список
                for (int k=0; k<9; k++)
                    if (masProgn[k]!=0)
                        field[i][j].prognosis.append(masProgn[k]);
            }
//            else
//                field[i][j].prognosis.clear(); // очищаем список
        }
    }
}

// исключить цифры из общего блока
void block(int x1, int x2, int y1, int y2, int masProgn[9], Cell field[9][9])
{
    for(int x=x1; x<x2; x++)
        for (int y=y1; y<y2; y++)
            for(int k=0; k<9; k++)
                if (masProgn[k]==field[x][y].value && masProgn[k]!=0)
                    masProgn[k]=0;
}

// Найти одинаковые значения по соседним столбцам и строкам
int findFourCoincidences(Cell field[9][9], int i, int j, int row1, int row2, int col1, int col2)
{
    int prognCount=field[i][j].prognosis.count();
    int ok=0;
    for (int p=0; p<prognCount; p++)
    {
        int prognVal=field[i][j].prognosis[p];
        for (int k=0; k<9; k++)
        {
            if (field[row1][k].value==prognVal)
                ok++;
            if (field[row2][k].value==prognVal)
                ok++;
            if (field[k][col1].value==prognVal)
                ok++;
            if (field[k][col2].value==prognVal)
                ok++;
        }
        if (ok==4)
            return prognVal;
        else
            ok=0;
    }
    return 0;
}


// исключить цифру из прогноза клеток блока
void excludeDigitBlock(int x1, int x2, int y1, int y2, int prognVal, Cell field[9][9])
{
    for(int x=x1; x<x2; x++)
        for (int y=y1; y<y2; y++)
            for(int k=0; k<9; k++)
                if (field[x][y].value==0)
                    field[x][y].removeCellFromPrognosis(prognVal);
}
