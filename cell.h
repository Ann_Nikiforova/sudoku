#ifndef CELL
#define CELL
#pragma once

#include <QList>

class Cell
{
public:
    int index; // Индекс клетки
    int value; // Значение клетки
    QList<int> prognosis; // Прогноз
    bool operator==(Cell&other); // Проверка равенства значения в клетках
    void removeCellFromPrognosis(int val); // Метод для удаления значения из прогноза
};

#endif // CELL

