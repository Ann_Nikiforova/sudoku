#include "test_openeddigits.h"
#include "ui_mainwindow.h"
#include <cell.h>
#include <QDebug>

test_openedDigits::test_openedDigits()
{

}

test_openedDigits::~test_openedDigits()
{

}


// Нет пустых клеток с единственной цифрой в прогнозе
void test_openedDigits::noEmptyCellsWithSingleDigitInPrognosis()
{
    int startMas[9][9]=
    {
        {8,6,3, 4,9,5, 1,2,7},
        {1,0,0, 3,6,7, 0,8,4},
        {4,0,0, 2,8,1, 0,3,6},
        {7,3,6, 5,4,9, 8,1,2},
        {9,1,8, 6,7,2, 4,5,3},
        {2,5,4, 8,1,3, 7,6,9},
        {3,4,1, 9,2,8, 6,7,5},
        {5,8,9, 7,3,6, 2,4,1},
        {6,0,0, 1,5,4, 3,9,8}
    };
    // создаём копию масива
    int copyStartMas[9][9];
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            copyStartMas[x][y]=startMas[x][y];

    Cell startMasCells[9][9];

    // заполняем массив клеток
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            startMasCells[x][y].value=startMas[x][y];
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.openedDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            QCOMPARE(startMas[x][y],copyStartMas[x][y]);
        }
}

// Только одна клетка с единственной цифрой в прогнозе
void test_openedDigits::onlyOneCell()
{
    int startMas[9][9]=
    {
        {8,6,3, 4,9,5, 1,2,7},
        {1,0,5, 3,6,7, 9,8,4},
        {4,9,7, 2,8,1, 5,3,6},
        {7,3,6, 5,4,9, 8,1,2},
        {9,1,8, 6,7,2, 4,5,3},
        {2,5,4, 8,1,3, 7,6,9},
        {3,4,1, 9,2,8, 6,7,5},
        {5,8,9, 7,3,6, 2,4,1},
        {6,7,2, 1,5,4, 3,9,8}
    };
    Cell startMasCells[9][9];

    // заполняем массив клеток
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            startMasCells[x][y].value=startMas[x][y];
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.openedDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

// Клетка находится на правой границе поля
void test_openedDigits::cellLocatedRightEdgeField()
{
    int startMas[9][9]=
    {
        {8,6,3, 4,9,5, 1,2,7},
        {1,2,5, 3,6,7, 9,8,4},
        {4,9,7, 2,8,1, 5,3,6},
        {7,3,6, 5,4,9, 8,1,2},
        {9,1,8, 6,7,2, 4,5,3},
        {2,5,4, 8,1,3, 7,6,0},
        {3,4,1, 9,2,8, 6,7,5},
        {5,8,9, 7,3,6, 2,4,1},
        {6,7,2, 1,5,4, 3,9,8}
    };
    Cell startMasCells[9][9];

    // заполняем массив клеток
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            startMasCells[x][y].value=startMas[x][y];
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.openedDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

// Клетка находится на нижней границе поля
void test_openedDigits::cellLocatedLowerBoundaryEdgeField()
{
    int startMas[9][9]=
    {
        {8,6,3, 4,9,5, 1,2,7},
        {1,2,5, 3,6,7, 9,8,4},
        {4,9,7, 2,8,1, 5,3,6},
        {7,3,6, 5,4,9, 8,1,2},
        {9,1,8, 6,7,2, 4,5,3},
        {2,5,4, 8,1,3, 7,6,9},
        {3,4,1, 9,2,8, 6,7,5},
        {5,8,9, 7,3,6, 2,4,1},
        {0,7,2, 1,5,4, 3,9,8}
    };
    Cell startMasCells[9][9];

    // заполняем массив клеток
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            startMasCells[x][y].value=startMas[x][y];
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.openedDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

// Клетка находится в правом нижнем углу поля
void test_openedDigits::cellIsInBottomRightCornerField()
{
    int startMas[9][9]=
    {
        {8,6,3, 4,9,5, 1,2,7},
        {1,2,5, 3,6,7, 9,8,4},
        {4,9,7, 2,8,1, 5,3,6},
        {7,3,6, 5,4,9, 8,1,2},
        {9,1,8, 6,7,2, 4,5,3},
        {2,5,4, 8,1,3, 7,6,9},
        {3,4,1, 9,2,8, 6,7,5},
        {5,8,9, 7,3,6, 2,4,1},
        {6,7,2, 1,5,4, 3,9,0}
    };
    Cell startMasCells[9][9];

    // заполняем массив клеток
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            startMasCells[x][y].value=startMas[x][y];
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.openedDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

// Несколько клеток с единственной цифрой в прогнозе (вариант 1)
void test_openedDigits::fewCells_1()
{
    int startMas[9][9]=
    {
        {8,6,3, 4,9,5, 1,2,7},
        {1,0,5, 3,6,7, 0,8,4},
        {4,9,7, 2,8,1, 5,3,6},
        {7,3,6, 5,4,9, 8,1,2},
        {9,1,8, 6,7,2, 4,5,3},
        {2,5,4, 8,1,3, 7,6,9},
        {3,4,1, 9,2,8, 6,7,5},
        {5,8,9, 7,3,6, 2,4,1},
        {6,7,0, 1,5,4, 3,9,8}
    };
    Cell startMasCells[9][9];

    // заполняем массив клеток
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            startMasCells[x][y].value=startMas[x][y];
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.openedDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

// Несколько клеток с единственной цифрой в прогнозе (вариант 2)
void test_openedDigits::fewCells_2()
{
    int startMas[9][9]=
    {
        {8,6,3, 4,9,5, 1,0,7},
        {1,2,5, 3,0,7, 9,8,4},
        {4,0,7, 2,8,1, 5,3,6},
        {7,3,6, 5,4,9, 8,1,2},
        {9,1,8, 0,7,2, 4,5,3},
        {2,0,4, 8,1,3, 0,6,0},
        {0,4,1, 9,2,0, 6,7,5},
        {5,8,9, 7,3,6, 2,4,1},
        {6,7,2, 1,5,4, 3,0,8}
    };
    Cell startMasCells[9][9];

    // заполняем массив клеток
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            startMasCells[x][y].value=startMas[x][y];
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.openedDigits(startMasCells); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=resultMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << resultMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

