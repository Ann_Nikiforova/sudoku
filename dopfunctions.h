#ifndef DOPFUNCTIONS
#define DOPFUNCTIONS
#pragma once

#include <cell.h>


// перенести значения из массива Клеток в массив цифр
void transferInArray(Cell masCells[9][9], int masDigits[9][9]);

// Вычислить координаты следующей клетки
// строка
int rowCalculate(int i, int j);
// столбец
int columnCalculate(int i, int j);
// Проверка на ошибки
bool isErrors (Cell field[9][9], int val, int i, int j);
bool findErrorInBlock(int x1, int x2, int y1, int y2, int val, Cell field[9][9]);
// Вычислить координаты предыдущей клетки
// строка
int rowPrevious(int i, int j);
// столбец
int columnPrevious(int i, int j);

#endif // DOPFUNCTIONS

