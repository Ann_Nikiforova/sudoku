#include "test_enumerationvaluesinprognosis.h"

test_enumerationValuesInPrognosis::test_enumerationValuesInPrognosis()
{

}

test_enumerationValuesInPrognosis::~test_enumerationValuesInPrognosis()
{

}

void test_enumerationValuesInPrognosis::var_1_easy()
{
    int startMas[9][9]=
    {
        {0,6,3, 4,0,5, 1,2,7},
        {1,0,5, 3,6,7, 9,0,4},
        {4,9,7, 2,8,1, 5,3,6},
        {7,3,6, 5,0,9, 8,1,2},
        {9,1,0, 6,7,2, 4,5,3},
        {2,5,4, 8,1,3, 7,6,0},
        {0,4,1, 9,2,8, 0,7,5},
        {5,8,9, 7,3,6, 2,4,1},
        {6,0,2, 1,5,4, 3,9,8}
    };
    Cell startMasCells[9][9];

    // заполняем массив клеток
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            startMasCells[x][y].value=startMas[x][y];
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.enumerationValuesInPrognosis(startMasCells, 0 , 0); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=idealMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << idealMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

void test_enumerationValuesInPrognosis::var_2_medium()
{
    int startMas[9][9]=
    {
        {0,6,3, 4,0,5, 1,2,7},
        {1,0,5, 0,6,7, 9,0,4},
        {0,0,7, 2,8,0, 0,3,0},
        {7,3,6, 5,0,9, 8,1,2},
        {0,1,0, 6,7,2, 4,5,3},
        {2,5,4, 0,1,3, 7,6,0},
        {0,0,1, 9,2,0, 0,0,5},
        {5,8,9, 7,3,6, 2,0,1},
        {6,0,2, 1,0,4, 3,9,8}
    };
    Cell startMasCells[9][9];

    // заполняем массив клеток
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            startMasCells[x][y].value=startMas[x][y];
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.enumerationValuesInPrognosis(startMasCells, 0, 0); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]!=idealMas[x][y])
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "|" << "ожидалась" << idealMas[x][y] << ", а не" << startMas[x][y];
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

void test_enumerationValuesInPrognosis::var_3_hard()
{
    int startMas[9][9]=
    {
        {0,0,0, 0,0,0, 0,0,0},
        {0,1,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,3,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 0,2,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,8,0, 0,0,0, 0,0,0},
        {0,0,0, 0,0,0, 0,0,0},
        {0,0,0, 2,0,0, 9,0,0}
    };
    Cell startMasCells[9][9];

    // заполняем массив клеток
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
            startMasCells[x][y].value=startMas[x][y];
    fillPrognosis(startMasCells); // заполняем прогноз

    MainWindow test;
    test.enumerationValuesInPrognosis(startMasCells, 0, 0); // вызов тестируемой функции

    transferInArray(startMasCells, startMas); // вывод результата функции в массив

    // Проверка
    int proverka=0;
    for(int x=0; x<9; x++)
        for(int y=0; y<9; y++)
        {
            if (startMas[x][y]==0)
            {
                qDebug() << "Клетка с координатами:" << "X =" << x << ", Y =" << y << "не заполнена";
                proverka++;
            }
        }
    QCOMPARE(proverka,0);
}

