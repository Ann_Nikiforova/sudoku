#ifndef WORKPROGNOSIS
#define WORKPROGNOSIS
#pragma once

#include <cell.h>
#include <workprognosis.h>

// заполнить прогноз
void fillPrognosis(Cell field[9][9]);
// исключить цифры из общего блока
void block(int x1, int x2, int y1, int y2, int masProgn[9], Cell field[9][9]);
// Найти одинаковые значения по соседним столбцам и строкам
int findFourCoincidences(Cell field[9][9], int i, int j, int row1, int row2, int col1, int col2);
// исключить цифру из прогноза клеток блока
void excludeDigitBlock(int x1, int x2, int y1, int y2, int prognVal, Cell field[9][9]);

#endif // WORKPROGNOSIS

