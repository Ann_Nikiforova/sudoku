#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#pragma once

#include <QMainWindow>
#include <QMessageBox>
#include <QList>
#include <cell.h>
#include <dopfunctions.h>
#include <workprognosis.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void openedDigits(Cell field[9][9]);
    void hiddenDigits(Cell field[9][9]);
    void enumerationValuesInPrognosis(Cell field[9][9], int i, int j);

private:
    Ui::MainWindow *ui;
    Cell field[9][9]={};
    bool isChangedTable=true;

    void loadDataCells(Cell field[9][9]);
    void outputResult(Cell field[9][9]);
    bool findErrors(Cell field[9][9]);
    bool isEmptyField(Cell field[9][9]);
    bool isSolvedSudoku(Cell field[9][9]);
    bool isErrorInBlock(int x1, int x2, int y1, int y2, int val, Cell field[9][9]);
    bool isOneDigitInCell(Cell field[9][9]);

private slots:
    void SolvingSudoku();
    void ClearTheValues();
};

#endif // MAINWINDOW_H
